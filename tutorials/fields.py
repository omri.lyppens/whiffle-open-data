import matplotlib.pyplot as plt
import xarray as xr
import s3fs

ds_a = xr.open_zarr(
    s3fs.S3Map(
        root="s3://whiffle-wins50-data/data/whiffle_wins50_2050_scenario_fields.zarr",
        s3=s3fs.S3FileSystem(anon=True),
    )
).sel(time="2020/01/01/18", height=100)

ds_a.info()

ds_b = xr.open_zarr(
    s3fs.S3Map(
        root="s3://whiffle-wins50-data/data/whiffle_wins50_2020_scenario_fields.zarr",
        s3=s3fs.S3FileSystem(anon=True),
    )
).sel(time="2020/01/01/18", height=100)


(ds_a["wind_speed"] - ds_b["wind_speed"]).plot.pcolormesh()

plt.show()
