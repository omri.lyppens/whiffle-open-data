import s3fs
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import cartopy.crs as ccrs
import pyproj

ds = xr.open_zarr(
    s3fs.S3Map(
        root="s3://whiffle-wins50-data/data/whiffle_wins50_2050_scenario_virtual_metmasts.zarr",
        s3=s3fs.S3FileSystem(anon=True),
    )
)

# list and plot metmasts
fig = plt.figure()
fig.set_size_inches(10, 10)
ax = plt.axes(projection=ccrs.UTM(zone="31"))
gl = ax.gridlines(draw_labels=True, x_inline=False, y_inline=False)
ax.coastlines(resolution="10m")

proj = pyproj.CRS.from_user_input(ds.attrs["projection"])
transformer = pyproj.Transformer.from_proj(proj, "EPSG:4326", always_xy=True)

ds_locs = ds[["x", "y", "metmast_identifier"]].load()

print("metmasts in dataset:")
for metmast in ds_locs["metmast_identifier"].values:
    ds_ = ds_locs.isel(index=np.where(ds_locs["metmast_identifier"] == metmast)[0])
    coordinate_utm = ds_["x"].values.item(), ds_["y"].values.item()

    coordinate_lonlat = transformer.transform(*coordinate_utm)

    print(f"{metmast} (center_coordinate = {coordinate_lonlat})")

    ax.plot(ds_["x"], ds_["y"], "o", color="orange", mew=0, ms=3)
    ax.text(coordinate_utm[0], coordinate_utm[1], metmast, fontsize=4)

# plot time series for particular metmast
ds = ds.sel(time=slice("2020/06/20", "2020/06/27"))
fig, ax = plt.subplots()
ds = ds.isel(index=np.where(ds["metmast_identifier"] == "K13a")[0])
ax.plot(ds["time"], ds["wind_speed"].sel(height=80.0))
ax.set_xlabel("time")
ax.set_ylabel("wind speed [m/s]")

plt.show()
