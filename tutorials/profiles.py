import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import s3fs
import cartopy.crs as ccrs
import pyproj

ds = xr.open_zarr(
    s3fs.S3Map(
        root="s3://whiffle-wins50-data/data/whiffle_wins50_2050_scenario_profiles.zarr",
        s3=s3fs.S3FileSystem(anon=True),
    )
)

ds.info()

# list and plot metmasts
fig = plt.figure()
fig.set_size_inches(10, 10)
ax = plt.axes(projection=ccrs.UTM(zone="31"))
gl = ax.gridlines(draw_labels=True, x_inline=False, y_inline=False)
ax.coastlines(resolution="10m")

proj = pyproj.CRS.from_user_input(ds.attrs["projection"])
transformer = pyproj.Transformer.from_proj(proj, "EPSG:4326", always_xy=True)

ds_locs = ds[["x", "y"]].load()

print("metmasts in dataset:")
for index in ds_locs["index"].values:
    ds_ = ds_locs.isel(index=index)
    coordinate_utm = ds_["x"].values.item(), ds_["y"].values.item()

    coordinate_lonlat = transformer.transform(*coordinate_utm)

    print(f"{index} (center_coordinate = {coordinate_lonlat})")

    ax.plot(ds_["x"], ds_["y"], "o", color="orange", mew=0, ms=3)
    ax.text(coordinate_utm[0], coordinate_utm[1], index, fontsize=4)

# plot daily mean profile for particular metmast
ds = ds.sel(time="2020/06/20")
fig, ax = plt.subplots()
ax.plot(
    ds["temperature_based_on_moist_static_energy"].sel(index=300).mean(dim="time"),
    ds["height"],
)

plt.show()
